src=project-$(1).ps # source file
psp=\\hspace{0.5cm} # package spacing
hsp=\\hspace{0.5cm} # horizontal spacing
fv=\\includegraphics{front.pdf}
bv=\\includegraphics{back.pdf}
single=1
ifdef single
pfv=\\rotatebox{-90}{$(fv)} # package front view
pbv=\\rotatebox{-90}{$(bv)} # package back view
else
psp=\\hspace{0.5cm}
pfv=$(fv)$(fv) # package front view
pbv=$(bv)$(bv) # package back view
endif
npv=$(psp)$(pfv)$(hsp)\\reflectbox{$(pbv)} # normal view
mpv=$(psp)\\reflectbox{$(npv)} # mirror view
pkv=$(npv)\\newpage$(mpv)
tex='\\documentclass[12pt,a4paper,final]{letter}\\usepackage[top=1.0cm,bottom=0.5cm,left=0.5cm,right=0.5cm]{geometry}\\usepackage{fancyhdr}\usepackage{graphicx}\\renewcommand{\\headrulewidth}{0pt}\\begin{document}\\pagestyle{fancy}\\cfoot{}$(pkv)\\end{document}'
pdf=ps2eps $(1); epstopdf $(patsubst %.ps,%.eps,$(1)) -o $(2); rm -f $(patsubst %.ps,%.eps,$(1))
out=echo $(tex) > $(1).tex; pdflatex $(1).tex; rm -f $(1).tex

all: print.pdf

print: print.pdf
	@evince $^

front.pdf: $(call src,Front)
	@echo Formatting front..
	@$(call pdf,$^,$@)

back.pdf: $(call src,Back)
	@echo Formatting back..
	@$(call pdf,$^,$@)

print.pdf: front.pdf back.pdf
	@echo Compiling print..
	@$(call out,print)

clean:
	rm -f *.pdf *.aux *.log *~ *.tex
	rm -f *.bak ./*cache*
